<?php
/**
 * Projet d'étude - Questionnaire
 * LPSIL IDSE
 * 
 * @version 1.0
 * @since 2017-10-05
 */
?>

<?php
// ***************************** //
// Importation Header :

require('extend/header.php');
// ***************************** //
?>
<main><div id="container">

    <!-- ******************** -->
    <!-- CONNECTION -->
    <div id="content">
        
        <div id="profil">     
                
            <h2>Connection à l'application</h2>
                        
            <div id="contentProfil">
                <div id="photoProfil"><img src="profil/photos/log.png" /></div>

                <div id="connection">
                    <form action="profil/main.login.php" method="post">
                        <label for="user">Utilisateur : </label><br>
                        <input id="login" type="text" name="login"><br>
                        <label for="user">Mot de passe : </label><br>
                        <input id="pwd" type="password" name="pwd"><br>
                        <input id="submit" type="submit" value="Connexion">
                        <input id="oublierMdp" type="button" value="Mot de passe oublié">
                        <br>
                        <input id="addUser" type="button" value="Nouvel utilisateur">
                    </form>
                </div>
                
                <div id="formOublierMdp">
                    <fieldset><legend>Mot de passe oublié</legend>
                        <form action="profil/oublier.profil.php" method="post">
                           <label>Votre email* : </label><input id="oubliEmail" type="text" name="oubliEmail" required><br>
                           <label>Votre nom d'utilisateur* : </label><input id="oubliNom" type="text" name="oubliNom" required><br>
                           <input id="add" type="submit" value="Envoyer">
                           <p class="consigne">* : Champs de texte obligatoire.</p>
                        </form>
                    </fieldset>
                </div>
                
            </div> 
                
        </div>

    </div>
    <!-- ******************** -->

</div></main>
<?php
// ***************************** //
// Importation Header :

require('extend/footer.php');
// ***************************** //
?>