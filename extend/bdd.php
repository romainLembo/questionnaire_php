<?php
// Afficher les erreurs à l'écran
ini_set('display_errors', 1);
// Enregistrer les erreurs dans un fichier de log
ini_set('log_errors', 1);
// Nom du fichier qui enregistre les logs (attention aux droits à l'écriture)
ini_set('error_log', dirname(__file__) . '/log_error_php.txt');
// Afficher les erreurs et les avertissements
error_reporting(E_ALL);
?>

<?php
/* -------------------------
	DEBUT : Connection BDD + Gestion erreurs
------------------------- */

try
{
	// Connection a la BDD
	$bdd = new PDO
	(
		/* Host + nom BDD: */'mysql:host=localhost; dbname=projetweb',
		/* Identifiant : */'root',
		/* Mot de passe : */'',
		/* Opérateur de résolution de portée : */array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)
	);
	
	// Caractères en UTF-8
	$bdd->exec("SET CHARACTER SET utf8");
}


// Gestion des Erreurs
catch (Exception $e)
{
	die('Erreur: ' . $e->getMessage());
}

/* -------------------------
	FIN : Connection BDD + Gestion erreurs
------------------------- */
?>
