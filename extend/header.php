<?php
// ***************** //
// Importation BDD :
require('extend/bdd.php');

// Chargement automatique des classes
spl_autoload_register(function ($classname) { require 'class/'.$classname.'.class.php'; });

// Gestionnaire des classes
$manager = new Manager($bdd);
// ***************** //
?>


<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Questionnaire</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
    
    <header>
        <!-- 
        HEADER : informations du site 
        --> 
        <div id="header">

            <!-- 
             LOGO-TITLE
             --> 
            <a href="index.php" title="Titre"><div id="title"></div></a>
            <!-- 
            FIN LOGO-TITLE
             -->

        </div>
        <!-- 
         FIN HEADER 
        -->
    </header>