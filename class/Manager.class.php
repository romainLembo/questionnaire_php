<?php

/**
 * @class A faire appel notamment dans les pages de traitements pour l'AJAX ou dans les pages d'interfaces.
 * @tutorial Cette classe gère les classes Question, Reponse, Theme, User.
 */

Class Manager
{
    /**
     * @var PDO De type PDO, cette variable-objet permet de se connecter à la BDD.
     * 
     * @tutorial Cette variable a été instanciée sur la page extend/bdd.php et sur les pages de traitements (dossiers main, admin, profil).
     */
    private $_bdd;
    
    // --------------------------------------------------- //
    // METHODES MAIN
    
    /**
     * @function Constructeur de la classe Manager permettant d'instancier des objet Manager afin de faire appel aux méthodes de cette classe.
     * 
     * @param PDO Variable-objet permettant de se connecter à la BDD.
     */
    public function __construct($bdd)
    {
        $this->setBdd($bdd);
    }
    
    /**
     * @function Permet au constructeur de la classe Manager de se connecter à une BDD.
     * 
     * @param PDO $bdd Variable-objet permettant de se connecter à la BDD.
     */
    public function setBdd(PDO $bdd)
    {
        $this->_bdd = $bdd;
    }
    // --------------------------------------------------- //
    
}


?>